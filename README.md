Яроклава-JS

Двуязычная клавиатура, в которой можно вводить латиницу в русской раскладке - для этого нужно нажать и удерживать пробел. 
Содержит несколько дополнительных значков. 

[Попробовать в веб-браузере](http://программирование-по-русски.рф/яроклава-js.яргт)

[Вариант для Windows (с программой AutoHotkey)](autohotkey/AutoHotkey.ahk)

[Раскладка для Windows, вместо пробела нужно нажимать правый Alt (AltGr)](https://budden73.livejournal.com/27328.html)

[Вариант для Linux (xkb), тестирован в Debian Stretch LXDE](https://bitbucket.org/budden/iaroklava-js/src/master/linux/Яроклава-для-debian-stretch-lxde.md "linux/Яроклава-для-debian-stretch-lxde.md")

Также в директории "устарело" можно найти раскладки для Windows, созданные с помощью Microsoft Keyboard Layout Creator. В них латиница и значки повешены на AltGr. Они не нужны, если используется AutoHotkey, и не рекомендуются для использования из-за проблем с горячими клавишами (см. прилагаемые файлы прочтимя). 

[Видеоролик](https://www.youtube.com/watch?v=7dLcjXPInjU)
